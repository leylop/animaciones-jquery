let parallax=document.querySelector('.parallax');
 let tituloParallax=document.querySelector('.tituloParallax');

function ScrollParallax(){
    let scrollTop = document.documentElement.scrollTop;
    parallax.style.transform = 'translateY(' + scrollTop * -0.7 + 'px)';
   tituloParallax.style.transform = 'translateY(' + scrollTop * 0.4 + 'px)';
}
window.addEventListener('scroll',ScrollParallax);