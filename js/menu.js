var menu = document.getElementById("menu");
var e = 0;

function mostrarOcultar(){
	if (e==1) {
		ocultar();
		e = 0;
	} else { 
		mostrar();
		e = 1;
	}
}

function ocultar(){
	menu.style.marginLeft = "-330px";
	menu.style.transitionDuration = ".5s";
	e = 0;
}

function mostrar(){
	menu.style.marginLeft = "0";
	menu.style.transitionDuration = ".5s";

	e = 1;
}